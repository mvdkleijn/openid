<?php
/*
 * OpenID plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the OpenID plugin for Wolf CMS.
 *
 * The OpenID plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
 */

/**
 * The OpenID plugin allows end users to login to the site with their OpenID.
 *
 * @package wolf
 * @subpackage plugin.openid
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */

Plugin::setInfos(array(
    'id'          => 'openid',
    'title'       => 'OpenID',
    'description' => 'Allows your users to login using OpenID.',
    'version'     => '1.0.0',
    'license'     => 'GPLv3',
    'author'      => 'Martijn van der Kleijn',
    'website'     => 'http://www.vanderkleijn.net/wolf-cms.html',
    'update_url'  => 'http://www.vanderkleijn.net/plugins.xml',
    'type'        => 'both',
    'require_wolf_version' => '0.6.0'
));

// Setup the controller.
Plugin::addController('openid', 'OpenID', 'administrator', false);

// Setup observers
Observer::observe('account_display_profile', 'returnProfileData');
Observer::observe('login_required', 'openIdLogin');
//Observer::observe('logout_requested', 'openIdLogout');
Observer::observe('user_edit_view_after_details', 'addEditUserLinks');

// Load classes.
AutoLoader::addFile('SimpleOpenID', PLUGINS_ROOT.'/openid/lib/SimpleOpenID.php');

// Setup routes.
Dispatcher::addRoute(array(
    '/openid' => '/plugin/openid/index',
    '/openid?:any' => '/plugin/openid/index'
   ));

// Setup connections to other plugins
if (Plugin::isEnabled('account')) {
    AccountController::registerAction(__('Connect OpenID account'), BASE_URL.'/openid/connect');
}

// Event Handlers
function returnProfileData() {
    $out = array();

    $out[__('OpenID accounts')] = __('&mdash; none &mdash;');

    return $out;
}

function openIdLogin($uri) {
    redirect('http://localhost/wolfcms/openid?redirect='.urlencode($uri));
}

function openIdLogout() {
    if (AuthUser::logout()) {
        Flash::set('success', __('Logout succeeded.'));
    }
}

function addEditUserLinks() {
    echo '<p>';
    echo '<a href="'.BASE_URL.'openid/connect" title="'.__('Allows you to login using your OpenID account.').'">'.__('Connect OpenID account').'</a>';
    echo '</p>';
}