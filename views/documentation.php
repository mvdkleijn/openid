<?php
/*
 * OpenID plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the OpenID plugin for Wolf CMS.
 *
 * The OpenID plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
 */

/**
 * The OpenID plugin allows end users to login to the site with their OpenID.
 *
 * @package wolf
 * @subpackage plugin.openid
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */
?>

<h1><?php echo __('Documentation'); ?></h1>
<p>
    The OpenID plugin allows administrators and end-users alike to login to a Wolf CMS system
    using their OpenID account. The plugin will only accept an OpenID account if it has previously
    been connected to an existing Wolf CMS account.
</p>
<h2><?php echo __('Requirements'); ?></h2>
<ul>
    <li>Wolf CMS release 0.7.0 or greater</li>
    <li>The PHP cURL extension</li>
</ul>