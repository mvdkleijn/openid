<style>
	#openid{
        width: 10%;
        margin: 0 auto;
	}
	#openid fieldset {
        width: 26em;
        padding: 0.5em;
		border: 1px solid gray;
		display: inline;
	}
	#openid, #openid INPUT {
		font-family: "Trebuchet MS";
		font-size: 12px;
	}
	#openid LEGEND {
		font-weight: bold;
		color: #FF6200;
		padding-left: 5px;
		padding-right: 5px;
	}
	#openid INPUT.openid_login {
        background: url(<?php echo PLUGINS_URI.'openid/'; ?>images/login-bg.gif) no-repeat;
        background-color: #fff;
        background-position: 0 50%;
        color: #000;
        padding-left: 18px;
        width: 220px;
        margin-right: 10px;
	}
	#openid A {
        color: silver;
	}
	#openid A:hover {
        color: #5e5e5e;
	}
</style>
<div id="openid">
<fieldset>
<legend>OpenID Login</legend>
<form action="<?php echo $openid_submit; ?>" method="post" onsubmit="this.login.disabled=true;">
<input type="hidden" name="openid_action" value="login">
<div>
    <input type="text" name="openid_url" class="openid_login" value="http://">
    <input type="submit" name="login" value="login &gt;&gt;">
</div>
<div><a href="http://www.myopenid.com/" class="link" >Get an OpenID</a></div>
</form>
</fieldset>
</div>