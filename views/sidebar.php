<?php
/*
 * OpenID plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the OpenID plugin for Wolf CMS.
 *
 * The OpenID plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
 */

/**
 * The OpenID plugin allows end users to login to the site with their OpenID.
 *
 * @package wolf
 * @subpackage plugin.openid
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */
?>

<p class="button"><a href="<?php echo get_url('plugin/openid/documentation'); ?>"><img src="<?php echo PLUGINS_URI; ?>openid/images/documentation.png" align="middle" /><?php echo __('Documentation'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/openid/settings'); ?>"><img src="<?php echo PLUGINS_URI; ?>openid/images/settings.png" align="middle" /><?php echo __('Settings'); ?></a></p>
<div class="box">
<h2><?php echo __('OpenID plugin');?></h2>
<p>
    The OpenID plugin allows administrators and end-users alike to login to a Wolf CMS system
    using their OpenID account.
</p>
<p>
    Homepage: <a href="http://www.vanderkleijn.net/wolf-cms/plugins/openid.html">Wolf CMS OpenID plugin</a><br/>
    Homepage: <a href="http://www.wolfcms.org/">Wolf CMS</a>
</p>
<p>
    <?php echo __('Plugin version'); ?>: 0.0.7 (2010-06-21)
</p>
</div>
