<?php
/*
 * OpenID plugin for Wolf CMS. <http://www.wolfcms.org>
 * Copyright (C) 2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of the OpenID plugin for Wolf CMS.
 *
 * The OpenID plugin for Wolf CMS is made available under the terms of the GNU GPLv3 license.
 * Please see <http://www.gnu.org/licenses/gpl.html> for full details.
*/

/**
 * The OpenID plugin allows end users to login to the site with their OpenID.
 *
 * @package wolf
 * @subpackage plugin.openid
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @version 1.0.0
 * @since Wolf version 0.7.0
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 License
 * @copyright Martijn van der Kleijn, 2010
 */

class OpenidController extends PluginController {

    function __construct() {
        AuthUser::load();
        if (defined('CMS_BACKEND')) {
            define('OPENID_VIEWS', 'openid/views');
            $this->setLayout('backend');
        }
        else {
            define('OPENID_VIEWS', '../../plugins/openid/views');
            $page = Page::findByUri(Plugin::getSetting('layout', 'openid'));
            $layout_id = $this->getLayoutId($page);

            $layout = Layout::findById($layout_id);
            $this->setLayout($layout->name);
        }
        $this->assignToLayout('sidebar', new View('../../plugins/openid/views/sidebar'));
    }

    private function getLayoutId($page) {
        if ($page->layout_id)
            return $page->layout_id;
        else if ($page->parent)
            return $this->getLayoutId($page->parent);
        else
            exit ('You need to set a layout!');
    }

    private function _checkLoggedIn() {
        if (!AuthUser::isLoggedIn())
            $this->display(OPENID_VIEWS.'/403');
    }

    public function content($part=false, $inherit=false) {
        if (!$part)
            return $this->content;
        else
            return false;
    }

    public function index() {
        if ($_POST['openid_action'] == "login") { // Get identity from user and redirect browser to OpenID Server
            $openid = new SimpleOpenID;
            $openid->SetIdentity($_POST['openid_url']);
            $openid->SetTrustRoot(URL_PUBLIC);
            $openid->SetRequiredFields(array('email','fullname'));
            //$openid->SetOptionalFields(array('dob','gender','postcode','country','language','timezone'));
            if ($openid->GetOpenIDServer()) {
                $openid->SetApprovedURL(URL_PUBLIC.'openid');  	// Send Response from OpenID server to this script
                $openid->Redirect(); 	// This will redirect user to OpenID Server
            } else {
                $error = $openid->GetError();
                //echo "ERROR CODE: " . $error['code'] . "<br>";
                //echo "ERROR DESCRIPTION: " . $error['description'] . "<br>";
                Flash::set('error', $error['code'].' - '.$error['description']);
                redirect(get_url());
            }
            exit;
        } else if($_GET['openid_mode'] == 'id_res') { 	// Perform HTTP Request to OpenID server to validate key
            $openid = new SimpleOpenID;
            $openid->SetIdentity($_GET['openid_identity']);
            $openid_validation_result = $openid->ValidateWithServer();
            if ($openid_validation_result == true) {
                // OK HERE KEY IS VALID
                Flash::set('success', 'Successfully logged in.');
                AuthUser::forceLogin('admin');
            } else if($openid->IsError() == true) {			// ON THE WAY, WE GOT SOME ERROR
                $error = $openid->GetError();
                Flash::set('error', $error['code'].' - '.$error['description']);
                redirect(get_url());
                //echo "ERROR CODE: " . $error['code'] . "<br>";
                //echo "ERROR DESCRIPTION: " . $error['description'] . "<br>";
            } else {											// Signature Verification Failed
                //echo "INVALID AUTHORIZATION";
                Flash::set('error', 'Invalid OpenID authorization.');
                redirect(get_url());
            }
        } else if ($_GET['openid_mode'] == 'cancel') { // User Canceled your Request
            Flash::set('warning', 'You cancelled the login action.');
            redirect(get_url());
            //echo "USER CANCELED REQUEST";
        } else {
            $openid_submit = URL_PUBLIC.'openid';

            $this->display(OPENID_VIEWS.'/login', array('openid_submit' => $openid_submit));
        }
    }

    public function documentation() {
        $this->display(OPENID_VIEWS.'/documentation');
    }

    /*
    public function settings() {
        $this->_checkLoggedIn();

        if (!User::hasPermission('administrator')) {
            $this->display(OPENID_VIEWS.'/403');
        }

        $this->display(OPENID_VIEWS.'/settings', array('settings' => Plugin::getAllSettings('account')));
    }
     *
    */

    /*
    function save() {
        if (isset($_POST['settings'])) {
            $settings = $_POST['settings'];
            foreach ($settings as $key => $value) {
                $settings[$key] = mysql_escape_string($value);
            }

            if (Plugin::setAllSettings($settings, 'account')) {
                Flash::set('success', __('The settings have been saved.'));
            }
            else {
                Flash::set('error', __('An error occured trying to save the settings.'));
            }
        }
        else {
            Flash::set('error', __('Could not save settings, no settings found.'));
        }

        redirect(get_url('plugin/forum/settings'));
    }
     *
    */
}